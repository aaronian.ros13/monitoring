// index.js
// Import required packages
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const inputRoutes = require('./routes/inputRoute.js');

// Create an instance of Express
const app = express();

// Define port
const port = process.env.PORT || 3001;

// Configure middleware
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Connect to MongoDB
mongoose
  .connect('mongodb+srv://admin:admin1234@303-rosales.yr7aumn.mongodb.net/?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Connected to MongoDB Atlas');
  })
  .catch((err) => {
    console.error('Error connecting to MongoDB Atlas', err);
  });


// Define routes
const inputRoute = require('./routes/inputRoute.js');
app.use('/api', inputRoute);

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
