const express = require('express');
const router = express.Router();
const { saveData, showData, editData } = require('../controllers/InputController.js');
const InputController = require("../controllers/InputController.js");

// Route to save data
router.post('/save', (req, res) => {
	InputController.saveData(req, res);
})

// Route to show saved data
router.get('/show', (req, res) => {
	InputController.showData(req, res);
})

// Route to edit data by order ID
router.put('/edit/:id', (req, res) => {
	InputController.editData(req, res);
})

router.delete('/delete/:id', (req, res) => {
	InputController.deleteData(req, res);
})


module.exports = router;
