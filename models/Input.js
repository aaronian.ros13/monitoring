const mongoose = require('mongoose');

const inputSchema = new mongoose.Schema({
    date: {
        type: String,
        required: [true, 'Date is required in MM-DD-YYYY format']
    },
    time: {
        type: String,
        required: [true, 'Time is required in 12-hour format with AM/PM']
    },
    bloodPressure: {
        type: String,
        required: [true, 'Blood Pressure is required']
    },
    sugar: {
        type: String,
        required: [true, 'Sugar is required']
    },
    temperature: {
        type: String,
        required: [true, 'Temperature is required']
    }
});

module.exports = mongoose.model('Input', inputSchema);
