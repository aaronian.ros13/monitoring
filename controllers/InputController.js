const Input = require('../models/Input.js'); // Update the import statement

// Controller to save data
exports.saveData = async (req, res) => {
    try {
        // Extract data from the request body
        const { date, time, bloodPressure, sugar, temperature } = req.body;

        // Create a new input instance
        const newInput = new Input({ // Update the variable name here
            date,
            time,
            bloodPressure,
            sugar,
            temperature,
        });

        // Save the input to the database
        await newInput.save(); // Update the variable name here

        res.status(201).json({ message: 'Data saved successfully', input: newInput }); // Update the variable name here
    } catch (error) {
        res.status(500).json({ error: 'Error saving data', message: error.message });
    }
};

// Controller to show saved data
exports.showData = async (req, res) => {
    try {
        // Fetch all inputs from the database
        const inputs = await Input.find(); // Update the variable name here

        res.status(200).json({ inputs }); // Update the variable name here
    } catch (error) {
        res.status(500).json({ error: 'Error fetching data', message: error.message });
    }
};

// Controller to edit data by input ID
exports.editData = async (req, res) => {
    try {
        // Extract data from the request body
        const { date, time, bloodPressure, sugar, temperature, totalAmount } = req.body;

        // Find the input by ID
        const input = await Input.findById(req.params.id); // Update the variable name here

        if (!input) {
            return res.status(404).json({ message: 'Input not found' }); // Update the variable name here
        }

        // Update the input fields
        input.date = date;
        input.time = time;
        input.bloodPressure = bloodPressure;
        input.sugar = sugar;
        input.temperature = temperature;
        input.totalAmount = totalAmount;

        // Save the updated input
        await input.save(); // Update the variable name here

        res.status(200).json({ message: 'Data updated successfully', input }); // Update the variable name here
    } catch (error) {
        res.status(500).json({ error: 'Error updating data', message: error.message });
    }
};

exports.deleteData = async (req, res) => {
    try {
        // Find the input by ID and delete it
        const input = await Input.findByIdAndDelete(req.params.id); // Update the variable name here

        if (!input) {
            return res.status(404).json({ message: 'Input not found' }); // Update the variable name here
        }

        res.status(200).json({ message: 'Data deleted successfully' });
    } catch (error) {
        res.status(500).json({ error: 'Error deleting data', message: error.message });
    }
};
